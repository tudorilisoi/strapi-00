module.exports = ({ env }) => {
    return {
        graphql: {
            endpoint: '/graphql',
            tracing: true,
            shadowCRUD: true,
            playgroundAlways: env('GQL_PLAYGROUND') === 'true',
            depthLimit: 7,
            amountLimit: 100,
        },


        // email: {
        //     provider: 'socketlabs',
        //     providerOptions: {
        //         serverId:env('SOCKETLABS_SERVER'), 
        //         injectionApiKey: env('SOCKETLABS_INJECT_KEY'),
        //     },
        //     settings: {
        //         // defaultFrom: 'tudorilisoi@gmail.com',
        //         // defaultReplyTo: 'tudorilisoi@gmail.com',
        //         // bcc: 'tudorilisoi@gmail.com',
        //         defaultFrom: 'no-reply@mobilecenta.com',
        //         defaultReplyTo: 'no-reply@mobilecenta.com',
        //         bcc: 'tudorilisoi@gmail.com',
        //     },
        // },



        email: {
            provider: 'gmail-oauth2',
            providerOptions: {
                nodemailer_default_from: env('GMAIL_OAUTH_USER'),
                nodemailer_default_replyto: env('GMAIL_OAUTH_USER'),
                username: env('GMAIL_OAUTH_USER'),
                clientId: env('GMAIL_OAUTH_CLIENT_ID'),
                clientSecret: env('GMAIL_OAUTH_CLIENT_SECRET'),
                refreshToken: env('GMAIL_OAUTH_REFRESH_TOKEN'),
            },
        },


        /*        
               email: {
                   provider: 'sendgrid',
                   providerOptions: {
                       apiKey: env('SENDGRID_API_KEY'),
                   },
       
                 
               },
        */

        /* email: {
            provider: 'nodemailer-smtp',
            providerOptions: {
                host: env('SMTP_HOST'),
                port: env('SMTP_PORT'),
                secure: false,
                auth: {
                    user: env('SMTP_USERNAME'),
                    pass: env('SMTP_PASSWORD'),
                },
                tls: {
                    rejectUnauthorized: false,
                },
                requireTLS: false,
                connectionTimeout: 30 * 1000, // 30 sec
            },

            
        }, */

        // ...
    }
}

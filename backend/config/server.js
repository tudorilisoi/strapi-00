module.exports = ({ env }) => {
  const host = env('APP_HOST', '127.0.0.1')
  const port = env.int('APP_PORT', 1337)
  return {
    admin: {
      auth: {
        secret: env('ADMIN_JWT_SECRET'),
      },
    },
    host,
    port,
    proxy: env.bool('IS_PROXIED', true),
    url: env('APP_URL') || `http://${host}:${port}`
  }
}

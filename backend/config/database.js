module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    /* "mongo": {
      "connector": "mongoose",
      "settings": {
         "uri": "mongodb://localhost:27017/mobilecenta",
      },
      "options": {
        "ssl": false,
      }
    }, */
    
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: 'localhost',
        port: 5432,
        database: env('DB'),
        username: env('DB_USERNAME'),
        password: env('DB_PASSWORD'),
        // url: env('STRAPI_DB', 'postgresql://tudor@localhost:5432/strapi-00'),
      },
      options: {
        useNullAsDefault: true,
        // debug: true,
        acquireConnectionTimeout: env.int('DATABASE_TIMEOUT_DEV', 100000),
        pool: env.json('DATABASE_POOL_DEV', {
          min: 0,
          max: 50,
          createTimeoutMillis: 30000,
          acquireTimeoutMillis: 600000,
          idleTimeoutMillis: 30000
        })
      },
    },
  },
});

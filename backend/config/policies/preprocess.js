'use strict';
const _ = require('lodash')

/**
 * `preprocess` policy.
 */


// controller->action regex config
// TODO make sure only authors delete comments

const config = {
  comment: {
    add: (ctx) =>
      setCtxAuthor(ctx, 'body.comment.author'),
    update: (ctx) =>
      setCtxAuthor(ctx, 'body.author', true),

  },
  vote: {
    add: (ctx) =>
      setCtxAuthor(ctx, 'body.vote.author'),
    update: (ctx) =>
      setCtxAuthor(ctx, 'body.author', true),
  },

  feature: {
    'create|update': (ctx) => {
      const { request: { body } } = ctx
      const { controller, action } = ctxRoute(ctx)

      // prevent editing attached comments
      strapi.log.info(`PREPROCESS: removing comments for ${controller}.${action}`)
      delete body.comments

      setCtxAuthor(ctx, 'body.author', action === 'update')
    }
  },

}




function ctxRoute(ctx) {
  const { request: { route: { controller, action } } } = ctx
  return { controller, action }
}

function deleteObjectPath(obj, path) {
  const parts = path.split('.')
  const key = parts.pop()
  const parentPath = parts.join('.')
  const parentObj = _.get(obj, parentPath)
  if (parentObj) {
    delete parentObj[key]
    return true
  }
  return false
}

function ensureAuthUser(ctx) {
  if (!ctx.state.user) {
    throw strapi.errors.badRequest(null, {
      errors: [
        { id: 'policies.preprocess', message: 'You must login before performing this action' },
      ],
    });
  }
}

// set or delete the author key in the request body
function setCtxAuthor(ctx, path, remove = false) {
  ensureAuthUser(ctx)
  const { controller, action } = ctxRoute(ctx)

  const { request, state: { user } } = ctx
  if (remove) {
    strapi.log.info(`PREPROCESS: Delete ${path}`)
    deleteObjectPath(request, path)
  } else {
    strapi.log.info(`PREPROCESS: Set author for ${controller}.${action} into ${path}`)
    _.set(request, path, user.id)
  }
}

module.exports = async (ctx, next) => {
  // Add your own logic here.
  const { request } = ctx
  const { route } = request
  const { action, controller } = route

  const ctlPath = `${route.controller}.${route.action}`

  //set the author as the current user
  const _conf = config[controller] || {}
  let configFn
  Object.keys(_conf).forEach(actionRe => {
    if (!configFn) {
      const re = new RegExp(`${actionRe}`)
      if (re.test(action)) {
        configFn = _conf[actionRe]
      }
    }
  })

  if (!configFn) {
    console.log(`Preprocess SKIP: ${ctlPath} does not match config`)
    return await next();
  }

  console.log(`Preprocess policy: ${ctlPath}`)

  configFn(ctx)
  // console.log(`request.body`, request.body)

  return await next();
};

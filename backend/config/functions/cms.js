const slugify = require('slugify');

// beforeCreate(data)
// beforeUpdate(params, data)
const generateSlug = async (...args) => {
    const data = args.length === 2 ? args[1] : args[0]
    strapi.log.info(`Slug behavior`)
    data.slug = slugify(data.title).toLowerCase();
    console.log(data.slug)
}

//arguments are LC functions
const composeLifecycleFunctions = (...functions) => {
    return (...args) => {
        functions.forEach(fn => fn.apply(null, args))
    }
}

module.exports = {
    // generateSlug,
    composeLifecycleFunctions,
    slugLifecycles: {
        beforeUpdate: generateSlug,
        beforeCreate: generateSlug,
    },
}

# Strapi application

A quick description of your strapi application

# Request lifecycle

Usual flow would be:
    User makes a request
    User is validated (JWT or whatever) in the users-permissions plugin
    Any policies are executed on the route in order (anything before the next())
    Controller is executed
    Service is called by controller
    Query is called by the service
    Query performs ORM database functions and returns the data
    Request chain starts working in reverse
    Once the controller returns any information the policies are then ran backwards for any code after the next()
    Data or any errors are returned to the user
    Policies are not something you would use to set permissions, they are in addition to the permissions.

const originalCtl = require('strapi-plugin-upload/controllers/Upload.js')

module.exports = {
    async upload(ctx) {
        const res = await originalCtl.upload(ctx)
        console.log(ctx.body)
        console.log(ctx.state.user)
        return res;

        // throw strapi.errors.badRequest(null, {
        //     errors: [
        //       { id: 'Upload.replace.single.XXX', message: 'Cannot replace a file with multiple ones' },
        //     ],
        //   });
    }
}

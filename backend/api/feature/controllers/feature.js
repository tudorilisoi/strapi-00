'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    // this gets picked up by the permissions plugin
    filteredcount: async (ctx) => {
        console.log(ctx.request.query)
        const params = ctx.request.query
        const count = await strapi.services.feature.count(params);
        return count
        // return 66
    },
    
};

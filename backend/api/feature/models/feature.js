'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#life-cycle-callbacks)
 * to customize this model
 */

const { slugLifecycles, composeLifecycleFunctions } = strapi.config.functions.cms

module.exports = {
    lifecycles: {
        beforeUpdate: composeLifecycleFunctions(slugLifecycles.beforeUpdate),
        beforeCreate: composeLifecycleFunctions(slugLifecycles.beforeCreate),
    },
};




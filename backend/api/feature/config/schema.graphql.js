module.exports = {
  query: `
    featuresFilteredCount(where: JSON): Int!
  `,
 
    resolver: {   
      Query: {
        featuresFilteredCount: {
          description: 'Return the count of restaurants',
          resolverOf: 'application::feature.feature.filteredcount',
          // resolver: async (obj, options, ctx) => {
          //   return await strapi.api.restaurant.services.restaurant.count(options.where || {});
          // },
          resolver: 'application::feature.feature.filteredcount'
        },
      },
      Mutation: {
        createFeature: {
          description: 'Create a new feature request',
          policies: ['global::preprocess'],
        },
        
      },
    },
  };
  




module.exports = {
    mutation: `
    addComment(
        contentTypeID:String!       
        contentID: ID!
        comment:CommentInput!
        ): InteractionsComment!    

    addVote(
        contentTypeID:String!       
        contentID: ID!
        vote:VoteInput!
        ): InteractionsVote!    
  `,
    resolver: {
        Mutation: {

            //comments  
            updateComment:{
                policies: [`global::preprocess`],
            },          
            deleteComment:{
                policies: [`global::preprocess`],
            },          
            addComment: {
                description: `Add comment`,
                policies: [`global::preprocess`],
                resolverOf: `plugins::interactions.comment.add`,
                resolver: `plugins::interactions.comment.add`,                
            },

            //votes
            updateVote:{
                policies: [`global::preprocess`],
            },          
            deleteVote:{
                policies: [`global::preprocess`],
            },          
            addVote: {
                description: `Add vote`,
                policies: [`global::preprocess`],
                resolverOf: `plugins::interactions.vote.add`,
                resolver: `plugins::interactions.vote.add`,                
            },
        }
    },
}

'use strict';
const { sanitizeEntity } = require('strapi-utils');


/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const voteService = () => strapi
    .plugins.interactions.services.vote

module.exports = {
    add: async (ctx) => {
        const { vote, contentTypeID, relation, contentID } = ctx.request.body
        const record = await voteService()
            .add({ vote, contentTypeID, relation, contentID })
        return sanitizeEntity(record, { model: strapi.plugins.interactions.models.vote })
    },
    create: () => {
        // TODO send BadRequest
    },
    delete: async (ctx) => {
        const { id } = ctx.params
        const record = await voteService().delete({ id })
        return sanitizeEntity(record, { model: strapi.plugins.interactions.models.vote })
    },
    update: async (ctx) => {
        const { body } = ctx.request
        const { id } = ctx.params
        const record = await voteService().update({ id, vote: body })
        return sanitizeEntity(record, { model: strapi.plugins.interactions.models.vote })
    },
};

'use strict';
const { sanitizeEntity } = require('strapi-utils');


/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const commentService = () => strapi
    .plugins.interactions.services.comment

module.exports = {
    add: async (ctx) => {
        const { comment, contentTypeID, relation, contentID } = ctx.request.body
        const record = await commentService()
            .add({ comment, contentTypeID, relation, contentID })
        return sanitizeEntity(record, { model: strapi.plugins.interactions.models.comment })
    },
    create: () => {
        // TODO send BadRequest
    },
    delete: async (ctx) => {
        const { id } = ctx.params
        const record = await commentService().delete({ id })
        return sanitizeEntity(record, { model: strapi.plugins.interactions.models.comment })
    },
    update: async (ctx) => {
        const { body } = ctx.request
        const { id } = ctx.params
        const record = await commentService().update({ id, comment: body })
        return sanitizeEntity(record, { model: strapi.plugins.interactions.models.comment })
    },
};

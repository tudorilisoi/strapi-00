'use strict';
const { sanitizeEntity } = require('strapi-utils')

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

const RELATION_NAME = 'votes'

const getRelatedContentTypes = () => {
    return [
        'feature',
    ]
}

module.exports = {
    update: async ({ id, vote }) => {
        return await strapi.query('vote', 'interactions').update({ id }, vote)
    },
    delete: async ({ id }) => {

        //delete related
        const promises = []
        const knex = strapi.connections.default
        getRelatedContentTypes().forEach(cType => {
            const model = strapi.query(cType).model;
            const assocTable = model.associations.find(a => a.alias === RELATION_NAME).tableCollectionName;
            const q = knex(assocTable)
            q.where('vote_id', id)
            promises.push(q.delete())
        })
        await Promise.all(promises)

        const record = await strapi.query('vote', 'interactions').delete({ id })
        return record
    },

    add:
        async ({ vote, contentTypeID, contentID, user }) => {

            //find parent model
            let parentRecord = await strapi.query(contentTypeID).findOne(
                { id: contentID }
            )
            console.log(`parentRecord`, parentRecord)
            parentRecord[RELATION_NAME] = parentRecord[RELATION_NAME] || []


            //find existing vote
            const model = strapi.query(contentTypeID).model;
            const assoc = model.associations.find(a => a.alias === RELATION_NAME)
            const assocTable = assoc.tableCollectionName;
            const table = 'votes'
            const fk = 'vote_id'
            const cTypeFK = `${contentTypeID}_id`
            const knex = strapi.connections.default
            const votesQuery = strapi.query('vote', 'interactions')
            let voteRecord
            // try {

            const q =
                knex('votes')
                    .innerJoin(assocTable, `${table}.id`, `${assocTable}.${fk}`)
                    .where(`${assocTable}.${cTypeFK}`, contentID)
                    .andWhere(`${table}.author`, vote.author)
                    .select([`${table}.id`, `${table}.type`])
                    .first()


            const data = await q
            if (data) {
                voteRecord = await votesQuery.findOne({ id: data.id })

                if (voteRecord.type === vote.type) {
                    throw strapi.errors.badRequest(null, {
                        errors: [
                            { id: 'addVote.noop', message: `Your vote didn't change!` },
                        ],
                    });
                }
                voteRecord.type = vote.type

                // vote already attached to the parent record
                await votesQuery.update(
                    { id: voteRecord.id },
                    voteRecord
                )
            } else {
                voteRecord = await votesQuery.create(vote)
                parentRecord[RELATION_NAME].push(voteRecord)

                // need to attach the vote to the parent record
                await strapi.query(contentTypeID).update(
                    { id: contentID },
                    parentRecord
                )
                //make sure we do not double insert when saving counters
                delete parentRecord[RELATION_NAME]

            }

            //update counters 
            const countQ = knex(assocTable)
                .innerJoin(table, `${table}.id`, `${assocTable}.${fk}`)
                .where(`${assocTable}.${cTypeFK}`, contentID)

            const upvotes = (await countQ.clone()
                .where(`${table}.type`, 'UP')
                .countDistinct(`${assocTable}.vote_id`))[0].count

            const downvotes = (await countQ.clone()
                .where(`${table}.type`, 'DOWN')
                .countDistinct(`${assocTable}.vote_id`))[0].count

            parentRecord = { ...parentRecord, upvotes, downvotes }

            await strapi.query(contentTypeID).update(
                { id: contentID },
                parentRecord
            )


            // } catch (error) {
            //     console.error(error)
            // }
            return voteRecord
        },
}


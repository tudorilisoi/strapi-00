'use strict';
const { sanitizeEntity } = require('strapi-utils')

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

const RELATION_NAME = 'comments'

const getRelatedContentTypes = () => {
    return [
        'feature',
    ]
}

module.exports = {
    update: async ({ id, comment }) => {
        return await strapi.query('comment', 'interactions').update({ id }, comment)
    },
    delete: async ({ id }) => {

        //delete related
        const promises = []
        const knex = strapi.connections.default
        getRelatedContentTypes().forEach(cType => {
            const model = strapi.query(cType).model;
            const assocTable = model.associations.find(a => a.alias === RELATION_NAME).tableCollectionName;
            const q = knex(assocTable)
            q.where('comment_id', id)
            promises.push(q.delete())
        })
        await Promise.all(promises)

        const record = await strapi.query('comment', 'interactions').delete({ id })
        return record
    },

    add:
        async ({ comment, contentTypeID, contentID, user }) => {
            // console.log(`ctx`, ctx.request.body)

            const newComment = await strapi.query('comment', 'interactions').create({
                ...comment,
                // created_by: user.id,
            });
            console.log(`newComment`, newComment)
            const parentRecord = await strapi.query(contentTypeID).findOne(
                { id: contentID }
            )
            console.log(`parentRecord`, parentRecord)
            parentRecord[RELATION_NAME] = parentRecord[RELATION_NAME] || []
            parentRecord[RELATION_NAME].push(newComment)

            await strapi.query(contentTypeID).update(
                { id: contentID },
                parentRecord
            )

            return newComment
        },
}


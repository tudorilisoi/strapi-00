module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'prettier', 'prettier/react'],
  env: {
    browser: true,
    es6: true,
    commonjs: true
  },
  parserOptions: {
    ecmaVersion: 2020,
    ecmaFeatures: { jsx: true }
  },

  globals: {
    "React": "readonly",
  },

  plugins: [
    'react',
    // 'prettier' mod
  ],
  rules: {
    // __ CORE
    'no-use-before-define': 0,
    'import/prefer-default-export': 0,
    'no-console': 'warn',
    'no-nested-ternary': 0,
    'no-underscore-dangle': 0,
    'no-unused-expressions': ['error', { allowTernary: true }],
    camelcase: 0,
    'react/self-closing-comp': 1,
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.js', 'jsx']
      }
    ],
    // __ REACT
    'react/prop-types': 0,
    'react/destructuring-assignment': 0,
    'react/jsx-no-comment-textnodes': 0,
    'react/jsx-props-no-spreading': 0,
    'react/no-array-index-key': 0,
    'react/no-unescaped-entities': 0,
    'react/require-default-props': 0,
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx", ".tsx"] }]
    // __ ACCESSIBILITY
    'jsx-a11y/label-has-for': 0,
    'jsx-a11y/anchor-is-valid': 0,
    // __ NEXT.JS
    'react/react-in-jsx-scope': 0,
    // __ PRETTIER
    // 'prettier/prettier': ['error', { singleQuote: true }]
    "import/extensions": [0, "never"],
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          // ['babel-polyfill', 'babel-polyfill/dist/polyfill.min.js'],
          // ['helper', './utils/helper'],
          // ['material-ui/DatePicker', '../custom/DatePicker'],
          ['~', './']
        ],
        extensions: ['.ts', '.js', '.jsx', '.tsx', '.json']
      }
    }
  }
}


/* eslint-disable jsx-a11y/anchor-has-content */
import MuiLink from '@material-ui/core/Link';
import clsx from 'clsx';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import React from 'react';

export const routes = [
    // pathhname regex, next route, folder(for getting active nav link)
    [`^/$`, '/', null],
    [`^/page/(\\d+)$`, '/page/[id]', null],
    [`^/features/(\\d+)$`, '/features/[id]', `^/features`],
]

export const getActiveRoute = (routerPathname, exact = true) => {
    return routes.find(r => {
        const [regexp, route, folderRegexp] = r
        return new RegExp((exact ? false : folderRegexp) || regexp).test(routerPathname)
    })
}

export const isURLActive = (routerPath, path, exact = true) => {
    if (exact) {
        return routerPath === path
    }
    return getActiveRoute(routerPath, exact) === getActiveRoute(path, exact)
}

const NextComposed = React.forwardRef(function NextComposed(props, ref) {
    const { as, href, ...other } = props;

    return (
        <NextLink href={href} as={as}>
            <a ref={ref} {...other} />
        </NextLink>
    );
});

NextComposed.propTypes = {
    as: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    prefetch: PropTypes.bool,
};

// A styled version of the Next.js Link component:
// https://nextjs.org/docs/#with-link
function Link(props) {
    let {
        href,
        // eslint-disable-next-line prefer-const
        activeClassName = 'active',
        // eslint-disable-next-line prefer-const
        className: classNameProps,
        // eslint-disable-next-line prefer-const
        innerRef,
        // eslint-disable-next-line prefer-const
        naked,
        // eslint-disable-next-line prefer-const
        as,
        // eslint-disable-next-line prefer-const
        ...other
    } = props;

    const router = useRouter();
    const pathname = typeof href === 'string' ? href : href.pathname;
    // console.log(`href: ${pathname}, router pathname: ${router.pathname}`)
    const className = clsx(classNameProps, {
        [activeClassName]: (router.pathname === pathname) && activeClassName,
    });


    // try to guess route pattern
    let asProp = as
    if (!asProp) {

        const r = getActiveRoute(pathname)
        if (r) {
            const [regex, route] = r
            // console.log(pathname, route)
            asProp = href
            href = route
        }
    }
    asProp = asProp || href

    if (naked) {
        return <NextComposed className={className} as={asProp} ref={innerRef} href={href} {...other} />;
    }

    return (
        <MuiLink component={NextComposed} className={className} as={asProp} ref={innerRef} href={href} {...other} />
    );
}

Link.propTypes = {
    children: PropTypes.any,
    activeClassName: PropTypes.string,
    as: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    className: PropTypes.string,
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
    naked: PropTypes.bool,
    onClick: PropTypes.func,
    prefetch: PropTypes.bool,
};

export default React.forwardRef((props, ref) => <Link {...props} innerRef={ref} />);

import { blueGrey, red } from '@material-ui/core/colors';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

// Create a theme instance.
const secondaryColor = '#77c425'
const primaryColor = '#007679'
// const headingsMargin = '.33em 0 .11em 0'
const headingsMargin = '0 0 .11em 0'
const mainFontFamily = 'Roboto, Helvetica, Arial, sans-serif'
const headingsFontFamily = 'Play, Helvetica, Arial, sans-serif'
const themeData = createMuiTheme({
    palette: {
        bg: { color: blueGrey['500'] },
        primary: {
            main: primaryColor,
        },
        secondary: {
            // NOTE .MuiTypography-colorPrimary overrides text color on buttons
            main: `${secondaryColor} !important`,
        },
        error: {
            main: red[900],
        },
        background: {
            default: '#fff',
        },
        text: {
            primary:
                'rgba(0, 0, 0, 0.87)',
            secondary:
                'rgba(0, 0, 0, 0.54)',
            disabled:
                'rgba(0, 0, 0, 0.38)',
            hint:
                'rgba(0, 0, 0, 0.38)',
        },
    },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                'body': {
                    fontFamily: mainFontFamily,
                },
                'h1+h2, h2+h3, h4+h4, h4+h5, p+h1, p+h2, p+h3, p+h4': {
                    marginTop: '1.33em !important',
                },
                a: {
                    color: primaryColor,
                },
                li: {
                    textAlign: 'left'
                },
            },
        },
    },
    typography: {
        fontDisplay: 'swap',
        // fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
        fontFamily: headingsFontFamily,
        htmlFontSize: 18,
        fontSize: 18,
        h1: {
            fontSize: `${2.2 * 1.1616}rem`,
            fontWeight: '400',
            // marginBottom: '.5em',
            margin: headingsMargin,

        },
        h2: {
            fontSize: `${1.6 * 1.1616}rem`,
            fontWeight: '400',
            margin: headingsMargin,
        },
        h3: {
            fontSize: `${1.4 * 1.1616}rem`,
            margin: headingsMargin,
        },
        h4: {
            fontSize: `${1.2 * 1.1616}rem`,
            margin: headingsMargin,
        },
        body1: {
            fontFamily: mainFontFamily,
        }

    },
});

const theme = responsiveFontSizes(themeData, {
    factor: 2, // font size on small screens
})

export default theme;

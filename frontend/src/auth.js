import React, { useEffect, useState, useMemo, useContext } from 'react'
import { eraseCookie, getCookie, setCookie } from "./cookies"


const authDefaults = { jwt: null, user: null }
export const AuthContext = React.createContext(authDefaults)



export function useAuth(cookieString) {
    return useContext(AuthContext)
}

/**
 * cookieString must come from req.headers
 * @param {string} cookieString 
 * @returns 
 */
export function _useAuth(cookieString) {
    const [login, setLogin] = useState(authDefaults)

    useEffect(() => {
        console.log('LOGIN IS', login)

    }, [login.jwt, cookieString])

    function _setLogin(data, forced = false) {
        if (forced || (data.jwt !== login.jwt)) {
            console.log(`🚀 ~ _setLogin ~ login.jwt`, data, login, forced)
            if (typeof window !== 'undefined') {
                if (data.jwt) {
                    setCookie('STRAPI_LOGIN', JSON.stringify(data))
                } else {
                    eraseCookie('STRAPI_LOGIN')
                }
                // debugger
            }
            setLogin(data)
        }
    }

    useEffect(() => {

        // browser
        if (typeof window !== 'undefined') {

            let storedData
            storedData = parseAuthCookie(document.cookie)
            console.log(`BROWSER storedData`, storedData)
            _setLogin(storedData)
        }

    },
        //[login.jwt, cookieString, writing]
        []
    )

    //server
    if (typeof window === 'undefined') {
        let storedData = parseAuthCookie(cookieString)
        console.log(`SERVER storedData`, storedData)
        _setLogin(storedData)
    }

    const getReturnValues = useMemo(() => ({
        login: _setLogin,
        logout: () => _setLogin(authDefaults),
        user: login.user,
        jwt: login.jwt,
    }), [login, cookieString])
    return getReturnValues

    /* const getReturnValues = () => ({
        login: _setLogin,
        logout: () => _setLogin(authDefaults),
        user: login.user,
        jwt: login.jwt,
    })
    return getReturnValues() */

}

function parseAuthCookie(cookie) {
    let result = getCookie('STRAPI_LOGIN', cookie)
    if (result) {
        try {
            result = JSON.parse(result)
        } catch (error) {
            result = null
        }
    }
    console.log(`parseAuthCookie ~ result`, result)

    return result || authDefaults
}

export const useIsAuthor = (model, authorRelation = 'author') => {

    const loginData = useAuth()
    if (!loginData.user) {
        return false
    }
    console.log(loginData, model)

    return loginData.user.id === model[authorRelation]?.id
}

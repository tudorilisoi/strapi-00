import { gql } from '@apollo/client'

export const PAGE_QUERY = gql`
  query Page($id: ID!) {
    page (id: $id) {
        id
        title
        body
        slug
      }
  }
`
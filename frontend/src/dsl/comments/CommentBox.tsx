import type { InteractionsComment } from "../../../graphql"

function CommentBox({ comment }: { comment: InteractionsComment }) {
    return (<p>
        {comment.body}
        {comment.author.username && ` posted by ${comment.author.username}`}
        </p>
        )
}

export { CommentBox }

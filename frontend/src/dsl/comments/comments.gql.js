
import {gql} from '@apollo/client'
import { AuthorFragment } from '../auth/auth.gql'

export const COMMENT_MUTATION = gql`
  mutation addComment( 
    $contentTypeID: String!
    $contentID: ID!
    $comment: CommentInput!) {
    addComment(
        contentTypeID: $contentTypeID,
        contentID: $contentID,
        comment: $comment
        ){
          id
          body
          author { 
            ...AuthorFragment
          }
        }
  }
  ${AuthorFragment}
`
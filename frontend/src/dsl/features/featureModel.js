import * as yup from 'yup'
import { getValidationErrors } from '../../helpers';

export const getFeatureSchema = () => {
    const schema = yup.object().shape({
        title: yup.string()
            .label('Title')
            .required(),
        body: yup.string()
            .label('Details')
            .required().min(20),
    })
    return schema
}

export const validateFeature = getValidationErrors.bind(null, getFeatureSchema())

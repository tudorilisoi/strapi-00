import { gql } from '@apollo/client'
import { AuthorFragment } from '../auth/auth.gql'

export const CommentFragment = gql`
    fragment CommentFragment on InteractionsComment {      
      id
      body
          author {
              ...AuthorFragment
          }     
    }
    ${AuthorFragment}
  `

export const FeatureBaseFragment = gql`
    fragment FeatureBaseFragment on Feature {
      
        id
        title
        body
        upvotes
        downvotes   
        author {
                   ...AuthorFragment
        } 
     
    }
    ${AuthorFragment}
  `

export const updateFeaturePayloadFragment = gql`
    fragment updateFeaturePayloadFragment on updateFeaturePayload {
      feature {
          ...FeatureBaseFragment
          comments {
          ...CommentFragment
          }
      }
    }
    ${FeatureBaseFragment}  
    ${CommentFragment} 
  `

export const FEATURE_MUTATION = gql`
  mutation UpdateFeature(
    $id:ID!,
    $data:editFeatureInput!
  ){
    updateFeature(
      input:{
        where:{id:$id}, 
        data:$data}
        ){
          ...updateFeaturePayloadFragment
         }
  }

  # ${FeatureBaseFragment}  
  # ${CommentFragment}  
  ${updateFeaturePayloadFragment}
`

export const FEATURE_QUERY = gql`
  query Feature($id: ID!) {
    feature (id: $id) {        
          ...FeatureBaseFragment
          comments {
          ...CommentFragment
          }

      }
  }
  ${FeatureBaseFragment}  
  ${CommentFragment}  
`

export const FEATURES_QUERY = gql`
  query Features($limit: Int! $start:Int!, $sort:String, $where:JSON) {
    featuresFilteredCount(where:$where)
    features (limit:$limit, start:$start, sort:$sort, where:$where) {
        
               ...FeatureBaseFragment

                    
               comments {
                ...CommentFragment
                }

      }
  }
  ${FeatureBaseFragment}
  ${CommentFragment}  
  
`


import { useMutation } from '@apollo/client'
import { IconButton } from '@material-ui/core'
import ThumbDown from '@material-ui/icons/ThumbDown'
import ThumbUp from '@material-ui/icons/ThumbUp'
import { useContext } from 'react'
import { VOTE_MUTATION} from '~/src/dsl/votes/votes.gql'
import { SnackContext } from "../../components/Snack"
import { getGQLErrorMessage } from "../../helpers"

export function VoteButton({ query, onSuccess, voteType, relation, contentTypeID, contentID }) {
    const [voteMutation] = useMutation(
        VOTE_MUTATION, {
        refetchQueries: [
            {
                query,
                variables: { id: contentID },
            }
        ],
        errorPolicy: 'none'
    });

    const { setSnackProps } = useContext(SnackContext)
    const Icon = voteType === 'UP' ? ThumbUp : ThumbDown
    const doVoteMutation = async () => {
        setSnackProps({})
        try {

            const { data: addVote } = await voteMutation({
                variables: {
                    voteType, relation, contentTypeID, contentID
                }
            })
            const message = 'Thank you!'
            const severity = 'success'

            setSnackProps({ message, severity })
            // Router.push('/')
        } catch (e) {
            console.log('ERR VOTE_MUTATION', e)
            const message = getGQLErrorMessage(e)
                || 'There was an error, please try again later'
            setSnackProps({ message, severity: 'error' })
        }
    }
    return (
        <IconButton onClick={doVoteMutation}><Icon color="primary" /></IconButton>
    )
}

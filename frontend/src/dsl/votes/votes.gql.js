import { gql } from '@apollo/client'
import { AuthorFragment } from '../auth/auth.gql'

export const VOTE_MUTATION = gql`
  mutation addVote( 
    $contentTypeID: String!
    $contentID: ID!
    $voteType: ENUM_INTERACTIONSVOTE_TYPE!) {
      addVote(
        contentTypeID: $contentTypeID,
        contentID: $contentID,
        vote: { type: $voteType }
        ){
          id
          type
        }
  }
  
`

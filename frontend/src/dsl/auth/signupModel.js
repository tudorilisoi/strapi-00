import * as yup from 'yup'
import { getValidationErrors } from '../../helpers';

export const getLoginSchema = () => {
    const schema = yup.object().shape({
        email: yup.string()
            .label('E-mail')
            .required()
            .email(),
        username: yup.string()
            .label('Username')
            .required().min(4).max(255),
        password: yup.string()
            .label('Password')
            .required().min(6),
        passwordConfirmation: yup.string()
            .label('Password confirmation')
            .required().min(6)
            .oneOf([yup.ref('password'), null], 'Passwords must match'),

    })
    return schema
}

export const validateSignup = getValidationErrors.bind(null, getLoginSchema())


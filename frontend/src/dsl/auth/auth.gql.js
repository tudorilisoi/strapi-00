import { gql } from '@apollo/client'


export const AuthorFragment = gql`
    fragment AuthorFragment on UsersPermissionsUser {
      
        id
        username   
        email   
        confirmed
         # broken on emailConfirmation so use errorPolicy :all in useQuery
        role {
          id
          name
        }
     
    }
  `

export const MeFragment = gql`
    fragment MeFragment on UsersPermissionsMe {
      
        id
        username
        email
        confirmed
        role {
          id
          name
        }
     
    }
  `

export const LoginPayloadFragment = gql`
    fragment LoginPayloadFragment on UsersPermissionsLoginPayload {
      jwt
      user {
        ...MeFragment
      }
    }
    ${MeFragment}
  `

export const LOGIN_MUTATION = gql`
  mutation Login($identifier: String!, $password: String!) {
      login(input: { identifier: $identifier, password: $password }) {
      ...LoginPayloadFragment
    }
  }
  ${LoginPayloadFragment}
`
export const FORGOT_PASSWORD_MUTATION = gql`
  mutation ForgotPassword($email: String!) {
    
      forgotPassword(email: $email){
        ok
      }
    
  }
`
export const RESET_PASSWORD_MUTATION = gql`
  mutation ResetPassword(
    $code: String!,
    $password: String!,
    $passwordConfirmation: String!
    ) {
    
    resetPassword(
      password: $password
      passwordConfirmation: $passwordConfirmation
      code: $code
    ){
      ...LoginPayloadFragment
    }    
  }
  ${LoginPayloadFragment}
`
export const CONFIRM_EMAIL_MUTATION = gql`
  mutation ConfirmEmail(
    $code: String!    
    ) {
    
      emailConfirmation(      
      confirmation: $code
    ){
      ...LoginPayloadFragment
    }    
  }
  ${LoginPayloadFragment}
`


export const ME_QUERY = gql`
  query Me {
    me {
        ...MeFragment
      }
  }
  ${MeFragment}
`

export const REGISTER_USER_MUTATION = gql`
mutation Register(
  $username: String!, 
  $email: String!, 
  $password: String!) {
    register(input: { 
      username: $username, 
      email: $email, 
      password: $password, 
    }) {
      ...LoginPayloadFragment
    }
  }
  ${LoginPayloadFragment}
`

import * as yup from 'yup'
import { getValidationErrors } from '~/src/helpers';

export const getResetPasswordSchema = () => {
    const schema = yup.object().shape({
        password: yup.string()
            .label('Password')
            .required().min(6),
            passwordConfirmation: yup.string()
            .label('Password confirmation')
            .required().min(6)
            .oneOf([yup.ref('password'), null], 'Passwords must match'),
    })
    return schema
}

export const validateNewPassword = getValidationErrors.bind(null, getResetPasswordSchema())

// export { getLoginSchema }
// export { validateLogin }


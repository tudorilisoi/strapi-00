import * as yup from 'yup'
import { getValidationErrors } from '~/src/helpers';

export const getLoginSchema = () => {
    const schema = yup.object().shape({
        email: yup.string()
            .label('E-mail')
            .required().min(4).max(255),
        password: yup.string()
            .label('Password')
            .required().min(6),

    })
    return schema
}

export const validateLogin = getValidationErrors.bind(null, getLoginSchema())

// export { getLoginSchema }
// export { validateLogin }


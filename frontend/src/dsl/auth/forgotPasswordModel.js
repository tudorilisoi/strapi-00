import * as yup from 'yup'
import { getValidationErrors } from '../../helpers';

export const getEmailSchema = () => {
    const schema = yup.object().shape({
        email: yup.string()
            .label('E-mail')
            .required()
            .email(),      

    })
    return schema
}

export const validateEmail = getValidationErrors.bind(null, getEmailSchema())

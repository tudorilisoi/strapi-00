import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { onError } from "@apollo/client/link/error";
import { getLocalLoginData } from './auth';
import { eraseCookie } from './cookies';

export default function createApolloClient(initialState, ctx) {
  // The `ctx` (NextPageContext) will only be present on the server.
  // use it to extract auth headers (ctx.req) or similar.
  const httpLink = new HttpLink({
    // see https://nextjs.org/docs/basic-features/environment-variables#exposing-environment-variables
    uri: `${process.env.NEXT_PUBLIC_BACKEND_URL}/graphql` || 'http://localhost:1337/graphql', // Server URL (must be absolute)
    credentials: 'same-origin', // Additional fetch() options like `credentials` or `headers`
  })
  let link = httpLink
  // console.log('ctx', ctx)
  // TODO write a test
  const authLink = setContext((gr, { headers }) => {

    const loginData = getLocalLoginData(ctx?.req.headers.cookie || '')

    if (!loginData || ['Login', 'Register'].includes(gr.operationName)) {
      return { headers }
    }

    return {
      headers: {
        ...headers,
        authorization: `Bearer ${loginData.jwt}`
      }
    }
  })
  link = authLink.concat(httpLink)

  const errLink = onError(({ graphQLErrors, networkError, operation, forward }) => {
    if (graphQLErrors)
      console.log(`errLink -> graphQLErrors`, graphQLErrors)
    try {
      const { statusCode } = graphQLErrors[0].extensions.exception.output
      console.log(`errLink -> statusCode`, statusCode)
      if (statusCode === 401) {
        eraseCookie('STRAPI_LOGIN')
        // modify the operation context with a new token
        const oldHeaders = {...operation.getContext().headers};
        delete oldHeaders.authorization
        operation.setContext({
          headers: {
            ...oldHeaders,
          },
        });
        console.warn('CLEARED BAD JWT COOKIE')
        // retry the request, returning the new observable
        return forward(operation);
      }
    } catch (error) {
      //noop
    }
  });

  link = errLink.concat(link)

  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link,
    cache: new InMemoryCache().restore(initialState),
  })
}


// finds a key deep withing an object using JSON stringify serializer

import { makeStyles } from "@material-ui/core"

export function findNestedObj(entireObj, keyToFind) {
    let foundObj
    const str = JSON.stringify(entireObj, (_, nestedValue) => {
        if (nestedValue && nestedValue[keyToFind]) {
            foundObj = nestedValue[keyToFind]
        }
        return nestedValue
    })
    console.log(str)
    return foundObj
}

export function getGQLErrorMessage(e) {
    const msgs = findNestedObj(e, 'messages')
    const errors = findNestedObj(e, 'errors')
    const messages = msgs || errors
    if (messages && messages.length) {
        return messages[0].message
    }
    return e.message
}

export const useFormStyles = makeStyles(theme => ({
    form: {
        '& .xMuiInputBase-root': {
            display: 'block',

        },
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            marginLeft: 0,
            marginRight: 0,
            // width: '100%',
            // display: 'block',
            // minWidth: '75%',
        },
    },
    btnRow: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        paddingBottom: theme.spacing(1),
        paddingTop: theme.spacing(1),
    },
    btn: {
        flexGrow: 0,
        flexShrink: 0,
    },
    showPassword: {
        cursor: 'pointer',
    },
    socialGridItem: {
        flexGrow: 1,
        maxWidth: '100%',
    }
}))

export const getValidationErrors = async (schema, values) => {
    try {
        await schema.validate(values, { abortEarly: false })
        return false
    } catch (err) {
        // console.log(err.name); // => 'ValidationError'
        // console.log(err); // => [{ key: 'field_too_short', values: { min: 18 } }]
        const errors = err.inner.reduce((acc, error) => {
            const { path, message } = error
            acc[path] = message
            return acc
        }, {})
        // might this fuck SSR?
        // console.log(errors)
        return errors
    }
}
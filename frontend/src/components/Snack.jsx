import { NoSsr, Snackbar } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import React, { useEffect, useState } from "react";

export default function Snack(props) {
  const { message, severity = "success" } = props
  const [open, setOpen] = React.useState(null);

  useEffect(() => {
    setOpen(!!message)
    console.log('eff')
    console.log(open, message, severity)
  }, [props])

  // const handleClick = () => {
  //   setOpen(true);
  // };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  return (<Snackbar
    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
    open={open && !!message} autoHideDuration={6000} onClose={handleClose}>
    <Alert
      elevation={6} variant="filled"
      onClose={handleClose} severity={severity}>
      {message}
    </Alert>
  </Snackbar>)
}

export const SnackContext = React.createContext({})
export const SnackProvider = ({ children }) => {
  const [props, setSnackProps] = useState({ message: null, severity: 'success' })
  const value = {
    snackProps: props,
    setSnackProps,
  }

  let timer
  useEffect(() => {

    if (props.message) {
      window.clearTimeout(timer)
      timer = window.setTimeout(() => {
        console.log('self-clearing snack')
        setSnackProps({})
      }, 6100)
    }

    return (() => {
      window.clearTimeout(timer)
    })

  }, [!!props.message])

  return <SnackContext.Provider value={value}>
    <NoSsr>
      <Snack {...props} />
    </NoSsr>
    {children}
  </SnackContext.Provider>
}

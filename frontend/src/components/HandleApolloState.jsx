import { Box, Typography } from "@material-ui/core"
import { Alert } from "@material-ui/lab"

export default function HandleApolloState({ loading, error }) {
    if (loading) {
        return (
            <Box m={4} >
                <Typography variant="h2">
                    Loading...
                </Typography>
            </Box>
        )
    }
    if (error) {
        return (
            <Box m={4} >
                <Alert severity="error">
                    <p>Cannot load page, please try again later</p>
                    {error}
                </Alert>
            </Box>
        )
    }
    return null
}
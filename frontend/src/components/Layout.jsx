import { AppBar, Box, Button, Drawer, Hidden, IconButton, List, ListItem, ListItemText, makeStyles, NoSsr, Toolbar, Typography, useTheme } from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import HomeIcon from '@material-ui/icons/Home';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import MenuIcon from '@material-ui/icons/Menu';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import { useRouter } from 'next/router';
import React, { useContext } from 'react';
import { useAuth, AuthContext } from '~/src/auth';
import Link, { isURLActive } from '../Link';
import Gravatar from './Gravatar';

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
    bgContainer: {
        background: theme.palette.bg.color,

    },
    navBtn: {
        color: `#fff !important`,
    },
    wrapperContainer: {
        background: '#eee',
        padding: 0,
        margin: '0 auto',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        [theme.breakpoints.up('sm')]: {
            maxWidth: 1280 + drawerWidth,
        },
        width: '100vw',

    },

    // the snackbar should align in regard to <main>
    '@global': {
        '.MuiSnackbar-root': {
            [theme.breakpoints.up('sm')]: {
                minWidth: '70%',
                paddingLeft: drawerWidth,
            },
        }
    },
    appAndDrawer: {
        display: 'flex',
        flex: 1,
        // position: 'relative',
    },
    app: {
        [theme.breakpoints.up('sm')]: {
            // width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        // order: 2,
        flex: 1,
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',

    },
    drawer: {
        // order: 1,
        // flex: 1,
        // flexGrow:0,
        position: 'fixed',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        height: '100vh',
        // left:'50%',
    },
    toolbar: {
        ...theme.mixins.toolbar,
        // backgroundColor:theme.palette.primary.main,
    },
    appBar: {
        marginBottom: theme.spacing(3),
    },
    menuTitle: {
        flexGrow: 1,
        marginLeft: theme.spacing(0),
    },
    mainContainer: {
        padding: 0,
        margin: 0,
        flex: '1',
        display: 'flex',
    },
    footer: {
        // backgroundColor: theme.palette.background.paper,
        marginTop: theme.spacing(1),
    },
    footerInner: {
        // backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(1),
    },

}))

export default function Layout(props) {
    const classes = useStyles()
    const loginData = useContext(AuthContext)
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const container = typeof window !== 'undefined' ? () => window.document.body : undefined;

    const userMenu = null
    return (

        <Box className={classes.bgContainer}>
            <Box className={classes.wrapperContainer}>
                <Box className={classes.appAndDrawer}>
                    <Box className={classes.app}>


                        <AppBar className={classes.appBar} position="static">
                            <Toolbar>
                                <Hidden smUp implementation="css">
                                    <IconButton
                                        color="inherit"
                                        aria-label="open drawer"
                                        edge="start"
                                        onClick={handleDrawerToggle}
                                        className={classes.menuButton}
                                    >
                                        <MenuIcon />
                                    </IconButton>
                                </Hidden>

                                <Box className={classes.menuTitle}>
                                    {/* <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                                <MenuIcon />
                            </IconButton> */}
                                    <Typography variant="h6" component="span" color="inherit">
                                        <Link color="inherit" href="/">
                                            {process.env.NEXT_PUBLIC_SITE_TITLE}
                                        </Link>
                                    </Typography>
                                </Box>
                                <NoSsr>
                                    {loginData.user ?
                                        <Button component={Link} href="/auth/login" color="inherit">
                                            <Hidden smDown implementation="css">
                                                {loginData.user.username}&nbsp;
                                            </Hidden>
                                            <Gravatar email={loginData.user.email} fullName={loginData.user.username} />
                                        </Button>
                                        :
                                        (
                                            <>
                                                <Button component={Link} href="/auth/login" className={classes.navBtn} >Login</Button>
                                                <Button component={Link} href="/auth/signup" className={classes.navBtn}>Signup</Button>
                                            </>
                                        )
                                    }
                                </NoSsr>

                                {userMenu}

                            </Toolbar>
                        </AppBar>
                        <Box component="main" id="main" className={classes.mainContainer}>
                            {props.children}
                        </Box>
                        <Footer />
                    </Box>
                    <nav className={classes.drawer} aria-label="Navigation">
                        <Hidden smUp implementation="css">
                            <Drawer
                                container={container}
                                variant="temporary"
                                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                                open={mobileOpen}
                                onClose={handleDrawerToggle}
                                classes={{
                                    paper: classes.drawerPaper,
                                }}
                                ModalProps={{
                                    keepMounted: true, // Better open performance on mobile.
                                }}
                            >
                                <DrawerMenu />
                            </Drawer>
                        </Hidden>
                        <Hidden xsDown implementation="css">
                            <Drawer
                                classes={{
                                    paper: classes.drawerPaper,
                                }}
                                variant="permanent"
                                open
                            >
                                <DrawerMenu />
                            </Drawer>
                        </Hidden>
                    </nav>
                </Box>

            </Box>
        </Box>

    )
}

function DrawerMenu() {
    const classes = useStyles()
    const router = useRouter()

    // default to exact:true unless stated
    const links = [
        {
            text: 'Home',
            icon: HomeIcon,
            pathname: '/',
        },
        {
            text: 'Getting started',
            icon: LibraryBooksIcon,
            pathname: '/page/2',
        },
        {
            text: 'Download',
            icon: CloudDownloadIcon,
            pathname: '/page/3',
        },
        {
            text: 'Feedback',
            icon: QuestionAnswerIcon,
            pathname: '/features',
            exact: false,
        },
    ]
    const [firstLinkSet, ...secondLinkSet] = links
    // console.log('route', getActiveRoute(router.asPath))

    return (
        <div>
            {/* <div className={classes.toolbar} /> */}
            {/* <Divider /> */}
            <List className={classes.toolbar}>
                {[firstLinkSet].map((info) => (
                    <ListItem
                        selected={isURLActive(router.asPath, info.pathname, info.exact === false ? info.exact : true)}
                        button key={info.pathname} href={info.pathname} component={Link}>
                        <ListItemIcon ><info.icon color="primary" /></ListItemIcon>
                        <ListItemText primary={info.text} />
                    </ListItem>
                ))}

            </List>
            {/* <Divider /> */}
            <List>
                {secondLinkSet.map((info) => (
                    <ListItem
                        selected={isURLActive(router.asPath, info.pathname, info.exact === false ? info.exact : true)}
                        button key={info.pathname} href={info.pathname} component={Link}>
                        <ListItemIcon ><info.icon color="primary" /></ListItemIcon>
                        <ListItemText primary={info.text} />
                    </ListItem>
                ))}
            </List>
        </div>
    )
}

function Footer() {
    const classes = useStyles()
    return (
        <footer className={classes.footer}>
            <hr />
            <div className={classes.footerInner}>
                <Typography variant="subtitle1" align="center" component="p">
                    <Link color="primary" href="/" >mobilecenta.com</Link>
                </Typography>
                <Copyright />
            </div>
        </footer>
    )
}

function Copyright() {
    return (
        <Typography variant="body2" color="textPrimary" align="center">
            {'Copyright © '}
            <Link color="primary" href="/">
                mobilecenta.com
        </Link >
            {' '}
            2018 - {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export const withLayout = (C) => {
    const name = `WithLayout-${C.displayName || C.name || 'Component'}`
    function WithLayout(props) {
        return (
            <Layout key="layout">
                <C key="page" {...props} />
            </Layout>
        )
    }
    WithLayout.displayName = name
    return WithLayout
}

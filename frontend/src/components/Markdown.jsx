import ReactMarkdown from 'react-markdown'
import { Typography } from '@material-ui/core'
import Link from '~/src/Link';

function Heading(props) {
    return <Typography variant={`h${props.level}`}>{props.children}</Typography>
}
function Paragraph(props) {
    return <Typography component="p">{props.children}</Typography>
}
function LinkMD(props) {
    return <Link href={props.href}>{props.children}</Link>
}

function Image(props) {
    // console.log(`img`, props)
    const { alt, ...otherProps } = props
    // extract style from the image
    let style = {}
    if (alt) {
        // match style enclosed by curlies
        const styleMatches = /\{.+\}/.exec(alt)
        if (styleMatches && styleMatches.length > 0) {
            style = `{${styleMatches[0]}}`
        }
    }
    // return <img {...otherProps} style={{ maxWidth: '100%' }} />
    // NOTE using "babel-plugin-styled-components" to allow passing text style as the css prop

    // eslint-disable-next-line jsx-a11y/alt-text
    return <img {...otherProps} css={style} />
}


function replaceUploads(markdownString) {
    return markdownString.replace('(/uploads', `(${process.env.NEXT_PUBLIC_BACKEND_URL}/uploads`)
}
const renderersMUI = {
    heading: Heading,
    paragraph: Paragraph,
    image: Image,
    link: LinkMD,
}
export default function Markdown(props) {
    const { source, ...otherProps } = props
    //   const {renderers, ...otherProps} = props
    const opts = {
        escapeHtml: false,
        renderers: renderersMUI,
    }
    if (source) {
        const alteredSource = source ? replaceUploads(source) : source
        opts.source = alteredSource
    }

    const finalProps = { ...opts, ...otherProps, }
    return <ReactMarkdown {...finalProps} />

}

import { getGravatarUrl } from 'react-awesome-gravatar';
import Avatar from '@material-ui/core/Avatar';

export default function Gravatar({ email, fullName, ...props }) {
    const profileUrl = getGravatarUrl(email, {default:'identicon'})
    return (
        <Avatar 
        alt={fullName}
        src={profileUrl} {...props}>
            {fullName ? fullName[0] : '?'}
        </Avatar>
    )

}
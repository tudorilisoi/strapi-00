import { Box, Button, Typography } from "@material-ui/core";
import Router from 'next/router';
import { useAuth } from "../auth";
import { eraseCookie } from "../cookies";
import { useFormStyles } from "../helpers";

export default function LoggedIn({ user }) {
    const classes = useFormStyles()
    const {logout} = useAuth()
    return (
        <>
            <Typography variant="h5" component="h2" gutterBottom >
                Logged in as <strong>{user.username}</strong>
            </Typography>
            <Box mt={0}>
                <Button
                    onClick={()=>{
                        logout()
                        Router.push('/', '', { shallow: false })
                    }}
                    size="large"
                    type="submit"
                    className={classes.btn}
                    variant="contained" color="primary">
                    Logout
            </Button>
            </Box>
        </>
    )
}

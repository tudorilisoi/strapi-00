import { ApolloClient, ApolloProvider, HttpLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { onError } from "@apollo/client/link/error"
import { concatPagination } from '@apollo/client/utilities'
import React, { useMemo } from 'react'
import { useApolloCacheController } from 'with-next-apollo-tree-walker'
import { eraseCookie } from './cookies'
import { useAuth } from '~/src/auth'

let apolloClient: ApolloClient<NormalizedCacheObject> | undefined
let globalJWT = null

interface initializeApolloProps {
  registerCache?: (cache: InMemoryCache) => void,
  initialState?: NormalizedCacheObject | undefined,
  jwt: string
}

function createApolloClient({ registerCache, initialState, jwt }: initializeApolloProps) {
  const cacheConfig = {
    typePolicies: {
      Query: {
        fields: {
          allPosts: concatPagination(),
        },
      },
    },
  }

  const cache = new InMemoryCache(cacheConfig).restore(initialState || {})

  if (registerCache) {
    registerCache(cache)
  }

  let link = new HttpLink({
    // see https://nextjs.org/docs/basic-features/environment-variables#exposing-environment-variables
    uri: `${process.env.NEXT_PUBLIC_BACKEND_URL}/graphql` || 'http://localhost:1337/graphql', // Server URL (must be absolute)
    credentials: 'same-origin', // Additional fetch() options like `credentials` or `headers`
  })

  const authLink = setContext((gr, { headers }) => {


    console.log(`🚀 ~ createApolloClient ~ jwt`, jwt)
    if (!jwt || ['Login', 'Register', 'ConfirmEmail'].includes(gr.operationName)) {
      return { headers }
    }

    return {
      headers: {
        ...headers,
        Authorization: `Bearer ${jwt}`
      }
    }
  })
  link = authLink.concat(link)

  const errLink = onError(({ graphQLErrors, networkError, operation, forward }) => {
    if (graphQLErrors)
      console.log(`errLink -> graphQLErrors`, graphQLErrors)
    try {
      const { statusCode } = graphQLErrors[0].extensions.exception.output
      console.log(`errLink -> statusCode`, statusCode)
      if (statusCode === 401) {
        eraseCookie('STRAPI_LOGIN')
        // modify the operation context with a new token
        const oldHeaders = { ...operation.getContext().headers };
        delete oldHeaders.authorization
        operation.setContext({
          headers: {
            ...oldHeaders,
          },
        });
        console.warn('CLEARED BAD JWT COOKIE')
        // retry the request, returning the new observable
        return forward(operation);
      }
    } catch (error) {
      //noop
    }
  });

  link = errLink.concat(link)

  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link,
    cache,
  })
}

function initializeApollo({ registerCache, initialState, jwt }: initializeApolloProps) {
  if (typeof window === 'undefined') {
    return createApolloClient({ registerCache, initialState, jwt })
  }


  // For SSG and SSR always create a new Apollo Client

  // Create the Apollo Client once in the client
  if (!apolloClient || globalJWT !== jwt) {
    apolloClient = createApolloClient({ registerCache, initialState, jwt })
    globalJWT = jwt
  }

  return apolloClient
}

export function useApollo(cacheId: string) {
  const ApolloCacheController = useApolloCacheController()
  const { jwt } = useAuth()
  console.log(`🚀 ~ useApollo ~ jwt`, jwt)

  const registerCache = React.useCallback((cache) => ApolloCacheController.registerCache(cacheId, cache), [ApolloCacheController, jwt]);

  return useMemo(() => initializeApollo({
    registerCache,
    initialState: ApolloCacheController.getExtractedCache(cacheId),
    jwt
  }), [registerCache])
}

export const withApollo = (PageComponent) => {
  const WithApollo = (props) => {
    const _apolloClient = useApollo('apollo-full-ssr')

    return (
      <ApolloProvider client={_apolloClient}>
        <PageComponent {...props} />
      </ApolloProvider>
    )
  }
  // return withApolloServerSideRender(WithApollo)
  return WithApollo
}

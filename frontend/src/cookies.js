import cookie from 'cookie'

export function setCookie(name, value, days) {
    let expires = "";
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = `; expires=${date.toUTCString()}`;
    }
    if (typeof document !== 'undefined') {
        document.cookie = `${cookie.serialize(name, value || "")}${expires}; path=/`;
    }
}
export function getCookie(name, source) {
    let defaultValue = ''
    if (typeof document !== 'undefined') {
        defaultValue = document.cookie
    }
    const cookies = cookie.parse(source || defaultValue)
    const value = (cookies || {})[name]
    return value || null;
}
export function eraseCookie(name) {
    if (typeof document !== 'undefined') {
        document.cookie = `${name}=; Max-Age=-99999999; path=/;`;
    }
}

export const GA_TRACKING_ID = process.env.NEXT_PUBLIC_GTAG_TRACKING_ID

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = (url) => {
    if (process.env.NODE_ENV !== 'production') {
        return null
    }
    window.gtag('config', GA_TRACKING_ID, {
        page_path: url,
    })
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }) => {
    if (process.env.NODE_ENV !== 'production') {
        return null
    }

    window.gtag('event', action, {
        event_category: category,
        event_label: label,
        value,
    })
}

export const GTag = () => {
    if (process.env.NODE_ENV !== 'production') {
        return null
    }
    return (
        <>
            <script
                async
                src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
            />
            <script
                // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{
                    __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `,
                }}
            />
        </>
    )
}

import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import Head from 'next/head';
import Router from 'next/router';
import React, { useEffect } from 'react';
// https://nextjs.org/docs/basic-features/built-in-css-support#adding-a-global-stylesheet
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { withApolloServerSideRender } from 'with-next-apollo-tree-walker';
import { SnackProvider } from '../src/components/Snack';
import * as gtag from '../src/gtag';
import theme from '../src/theme';
import { _useAuth, AuthContext } from '~/src/auth'




function MyApp(props) {
  const { Component, pageProps } = props;
  // const apolloClient = useApollo('apollo-full-ssr')

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);


  useEffect(() => {
    // Gtag/GA
    const handleRouteChange = (url) => {
      gtag.pageview(url)
    }
    Router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      Router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [])

  return (
    <>
      <Head key="head">
        <title>{process.env.NEXT_PUBLIC_SITE_TITLE}</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <AuthContext.Provider value={_useAuth(props.cookieHeader)}>
        <ThemeProvider theme={theme}>
          <SnackProvider >
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            {/* <ApolloProvider client={apolloClient} > */}
            <Component {...pageProps} />
            {/* </ApolloProvider> */}
          </SnackProvider>
        </ThemeProvider>
      </AuthContext.Provider>
    </>
  );
}

// MyApp.propTypes = {
//   // Component: PropTypes.elementType.isRequired,
//   // pageProps: PropTypes.object.isRequired,
// };

const App = withApolloServerSideRender(MyApp)

//pass cookie headers to MyApp
// App.getInitialProps = (context) => {
//   console.log(`🚀 ~ context.ctx.req?.headers.cookie`, context.ctx.req?.headers.cookie)
//   return {
//     cookieHeader: context.ctx.req?.headers.cookie
//   }
// }

export default App

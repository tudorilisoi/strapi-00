import { useQuery } from '@apollo/client'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import { withLayout } from '~/src/components/Layout'
import { withApollo } from '~/src/apolloUtils'
import HandleApolloState from '../src/components/HandleApolloState'
import Markdown from '../src/components/Markdown'
import { PAGE_QUERY } from '../src/dsl/pages/pages.gql'
// import { withApolloServerSideRender } from 'with-next-apollo-tree-walker';

function HomePage() {
    return <Index />
}

function Index() {
    const { loading, data, error } = useQuery(PAGE_QUERY, {
        variables: { id: 1 },
        // ssr:true,
        // fetchPolicy:'cache-and-network',
        notifyOnNetworkStatusChange: true,
    })
    // console.log(`pq`, loading, data, error)

    if (loading || error) {
        return <HandleApolloState {...{ loading, error }} />
    }
    const { page } = data
    return (
        <Container maxWidth="lg">
            <Box my={4} textAlign="center">

                <Markdown source={page.body} />
            </Box>
        </Container>
    )
}

export default withApollo(withLayout(Index))
// export default withApolloServerSideRender(Index)
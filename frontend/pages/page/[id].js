import { useQuery } from '@apollo/client'
import { Typography } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import { useRouter } from 'next/router'
import { withApollo } from '~/src/apolloUtils'
import HandleApolloState from '~/src/components/HandleApolloState'
import { withLayout } from '~/src/components/Layout'
import Markdown from '~/src/components/Markdown'
import { PAGE_QUERY } from '~/src/dsl/pages/pages.gql'


function Page() {
    const router = useRouter()
    const { id } = router.query
    const { loading, data, error } = useQuery(PAGE_QUERY, { variables: { id } })
    console.log(`pq`, router)

    if (loading || error) {
        return <HandleApolloState {...{ loading, error }} />
    }
    const { page } = data
    return (
        <Container maxWidth="lg">
            <Box my={0}>
                <Typography variant="h1">
                    {page.title}
                </Typography>
                <Markdown source={page.body} />
            </Box>
        </Container>
    )
}

export default withApollo(withLayout(Page))
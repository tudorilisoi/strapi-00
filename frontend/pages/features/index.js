import { useQuery } from '@apollo/client'
import { NoSsr, Paper, Typography } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import { useRouter } from 'next/router'
import { withApollo } from '~/src/apolloUtils'
import { useAuth } from '~/src/auth'
import HandleApolloState from '~/src/components/HandleApolloState'
import { withLayout } from '~/src/components/Layout'
import { FEATURES_QUERY } from '~/src/dsl/features/features.gql'
import Link from '~/src/Link'
import Gravatar from '../../src/components/Gravatar'

function FeatureBox({ feature }) {
    return (
        <Box key={feature.id} my={4}>
            <Paper>
                <Box p={2} display="block">
                    <Box display="flex" alignItems="center">
                        <Box flex="1" flexGrow="1">

                            <Typography variant="h1" component={Link}
                                as={`/features/${feature.id}`}
                                href="/features/[id]">
                                #{feature.id} {feature.title}
                            </Typography>
                        </Box>
                        <Box flex="1" flexGrow="0">
                            <Gravatar email={feature.author.email} fullName={feature.author.username} />
                        </Box>
                    </Box>
                    <Box component="p" display="flex" alignItems="center">
                        <span>{feature.upvotes} votes up, {feature.downvotes} votes down</span>

                    </Box>

                </Box>
            </Paper>
        </Box>
    )
}

function FeaturesPage() {
    const router = useRouter()
    const loginData = useAuth()

    console.log(`router.query`, router.query)

    const { limit, start } = { start: 0, ...router.query, limit: 20 }
    const { loading, data, error } = useQuery(FEATURES_QUERY, {
        variables: {
            // NOTE this works
            // where: { author: { id: 10 } },
            limit,
            start,
            sort: "upvotes:DESC, downvotes:ASC",
        }
    })
    // console.log(`pq`, loading, data, error)

    if (loading || error) {
        return <HandleApolloState {...{ loading, error }} />
    }
    const loginLink = (
        <NoSsr>
            {loginData.user ? null : (
                <Typography variant="h2">
                    Please login or signup to enable voting and adding feature requests
                </Typography>
            )}
        </NoSsr>
    )


    const { features, featuresFilteredCount } = data
    return (
        <Container maxWidth="lg">
            <Typography variant="h1">
                Feature requests ({featuresFilteredCount})
            </Typography>
            {loginLink}
            {features.map(feature => FeatureBox({ feature }))}
        </Container>
    )

}

export default withApollo(withLayout(FeaturesPage))
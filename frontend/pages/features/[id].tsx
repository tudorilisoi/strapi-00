import { useMutation, useQuery } from '@apollo/client'
import { Button, NoSsr, Paper, Typography } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import { TextField } from 'mui-rff'
import { useRouter } from 'next/router'
import { useContext, useEffect, useState } from 'react'
import { Form } from 'react-final-form'
import { withApollo } from '~/src/apolloUtils'
import { useIsAuthor, useAuth } from '~/src/auth'
import HandleApolloState from '~/src/components/HandleApolloState'
import { withLayout } from '~/src/components/Layout'
import Markdown from '~/src/components/Markdown'
import { validateFeature } from '~/src/dsl/features/featureModel'
import { FEATURE_QUERY, FEATURE_MUTATION } from '~/src/dsl/features/features.gql'
import Link from '~/src/Link'
import Gravatar from '../../src/components/Gravatar'
import { VoteButton } from '../../src/dsl/votes/VoteButton'
import { RichEditor } from '../../src/components/RichEditor'
import { Feature } from '../../graphql'
import { CommentBox } from '../../src/dsl/comments/CommentBox'
import { getGQLErrorMessage, useFormStyles } from '~/src/helpers'
import { SnackContext } from '~/src/components/Snack'

function FeatureDetailPage({ apolloClient }) {
    const router = useRouter()
    const loginData = useAuth()
    const { id } = router.query
    const { loading, data, error } = useQuery(FEATURE_QUERY, { variables: { id } })
    const [editing, _setEditing] = useState(false)

    function setEditing(value) {
        _setEditing(value)
        if (typeof window !== 'undefined') {
            let hash = value ? 'edit' : ''
            window.location.hash = hash
        }
    }

    // console.log(`pq`, loading, data, error)
    useEffect(() => {
        if (typeof window !== 'undefined' && window.location.hash === '#edit') {
            setEditing(true)
        }
    }, [])


    if (loading || error) {
        return <HandleApolloState {...{ loading, error }} />
    }
    const loginLink = (
        <NoSsr>
            {loginData.user ? null : (
                <Typography variant="h2">
                    Please login or signup to enable voting and adding feature requests
                </Typography>
            )}
        </NoSsr>
    )
    const { feature } = data
    const props = {
        feature,
        editing,
        setEditing,
    }
    const component = editing ? (
        <FeatureEditor {...props} />
    ) : (
        <FeatureBox {...props} />
    )
    return (
        <Container maxWidth="lg">
            <Typography variant="h1">
                <Link href="/features">Feature requests</Link>
            </Typography>
            {loginLink}
            {component}
        </Container>
    )

}

function FeatureEditor({ feature, setEditing }) {
    const classes = useFormStyles();
    const { setSnackProps } = useContext(SnackContext)

    const render = (formProps) => {
        console.log(`🚀 ~ render ~ formRenderProps`, formProps)
        const { feature,
            editing,
            form,
            setEditing,
            handleSubmit,
            submitting,
            pristine,
            values,
            invalid } = formProps
        const btnText = submitting ? 'One moment...' : 'Save'
        return (
            <form className={classes.form} onSubmit={handleSubmit} noValidate>
                <Box key={feature.id} my={4} >
                    <Paper>
                        <Box p={2}>
                            <Box p={0} display="block">
                                <TextField
                                    // autoFocus
                                    disabled={submitting}
                                    autoComplete="false"
                                    variant="outlined" label="title" name="title" required />
                            </Box>
                            <Box p={0} display="block">
                                <TextField
                                    // autoFocus
                                    multiline
                                    disabled={submitting}
                                    autoComplete="false"
                                    variant="outlined" label="description" name="body" required />
                            </Box>
                            <Box mt={2} display="flex" justifyContent="space-between">
                                <Button disabled={!pristine && (submitting)}
                                    size="large"
                                    type="submit"
                                    className={classes.btn}
                                    variant="contained" color="primary">
                                    {btnText}
                                </Button>

                                <Box>
                                    <Button
                                        disabled={!(formProps.modified || formProps.modifiedSinceLastSubmit) || pristine || submitting}
                                        onClick={form.reset}
                                        type="reset"
                                        size="small"
                                        variant="text" color="primary"
                                        className={classes.btn}
                                    >
                                        {'Reset'}
                                    </Button>
                                    <Button
                                        disabled={(submitting)}
                                        onClick={() => setEditing(false)}
                                        size="small"
                                        variant="text" color="primary"
                                        className={classes.btn}
                                    >
                                        {'Close'}
                                    </Button>
                                </Box>
                            </Box>
                        </Box>
                    </Paper>
                </Box>
            </form>
        )
    }
    const { title, body, id } = feature

    const [mutation, mutationState] = useMutation(FEATURE_MUTATION, { errorPolicy: 'none' });

    async function onSubmit(values) {
        console.log(`🚀 ~ onSubmit ~ values`, values)
        setSnackProps({})
        try {

            const { errors, data: { updateFeature } } = await mutation({
                variables: {
                    id,
                    data: values
                }
            })

            setSnackProps({ message: 'Feature updated', severity: 'success' })


        } catch (e) {
            console.log('ERR FEATURE_MUTATION', e)
            const message = getGQLErrorMessage(e)
                || 'There was an error, please try again later'
            setSnackProps({ message, severity: 'error' })
        }
    }

    const formProps = {
        feature,
        setEditing,
        validate: validateFeature,
        initialValues: { title, body },
        onSubmit,
        render,
    }
    return (
        <Form {...formProps} />
    )
}



function FeatureBox({ feature, editing, setEditing, ...props }: { feature: Feature, editing: Boolean }) {


    const { title, body } = feature
    const isAuthor = useIsAuthor(feature)
    function toggleEdit() {
        setEditing(!editing)
    }
    return (
        <Box key={feature.id} my={4}>
            <Paper>
                <Box p={2} display="block">
                    <Box display="flex" alignItems="center">
                        <Box flex="1" flexGrow="1">
                            <Typography variant="h1" >
                                #{feature.id} {title}
                            </Typography>
                        </Box>
                        <Box flex="1" flexGrow="0" display="flex">
                            <Gravatar email={feature.author.email} fullName={feature.author.username} />
                            <span>
                                {!isAuthor ? null : <Button onClick={toggleEdit}>{editing ? 'Cancel' : 'Edit'}</Button>}
                            </span>
                        </Box>
                    </Box>
                    <Box display="flex" justifyContent="space-between">
                        <span>
                            Created by <strong>{feature.author.username}</strong>
                        </span>
                    </Box>
                    <Box my={1}>
                        {body}
                    </Box>
                    <FeatureVotes feature={feature} />
                    <Box>
                        {feature.comments.map((c) => {
                            return (
                                <CommentBox key={c.id} comment={c} />
                            )
                        })}
                    </Box>
                </Box>
            </Paper>
        </Box>
    )


}

function FeatureVotes({ feature }: { feature: Feature }) {
    const voteProps = {
        query: FEATURE_QUERY,
        contentTypeID: 'feature',
        contentID: feature.id,
        relation: 'votes',
        voteType: null,
        onSuccess: (data) => {
            console.log(`Vote data:`, data)
        }
    }
    return (
        <Box display="flex" alignItems="center">
            <span>{feature.upvotes}</span><VoteButton {...{ ...voteProps, voteType: 'UP' }} />
            <span>{feature.downvotes}</span><VoteButton {...{ ...voteProps, voteType: 'DOWN' }} />

        </Box>
    )
}





export default withApollo(withLayout(FeatureDetailPage))
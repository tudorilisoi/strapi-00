import { useMutation } from '@apollo/client';
import { Button } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { TextField } from 'mui-rff';
import Head from 'next/head';
import { useContext, useState } from 'react';
import { Form } from 'react-final-form';
import { withLayout } from '~/src/components/Layout';
import { withApollo } from '~/src/apolloUtils';
import { SnackContext } from '../../src/components/Snack';
import { FORGOT_PASSWORD_MUTATION } from '../../src/dsl/auth/auth.gql';
import { getGQLErrorMessage, useFormStyles } from '../../src/helpers';
import { validateEmail } from '../../src/dsl/auth/forgotPasswordModel';

function ForgotPasswordPage() {

    return (
        <Box alignSelf="center" width="100%">
            <Container maxWidth="sm" >
                <Box my={4} >
                    <Typography variant="h4" component="h1" gutterBottom >
                        Reset your password
                    </Typography>
                    <ForgotPasswordForm />
                </Box>
            </Container>
        </Box>
    )
}

function ForgotPasswordForm() {
    const [resetPasswordMutation] = useMutation(FORGOT_PASSWORD_MUTATION, { errorPolicy: 'none' });
    const classes = useFormStyles();

    const { setSnackProps } = useContext(SnackContext)
    const [submitSuccess, setSubmitSuccess] = useState(false)

    const initialValues = { email: '' }
    async function onSubmit(values) {
        setSnackProps({})
        try {

            const { data } = await resetPasswordMutation({
                variables: {
                    email: values.email,
                }
            })
            setSnackProps({ message: 'Reset e-mail sent, please check your inbox', severity: 'success' })
            setSubmitSuccess(true)
        } catch (e) {
            console.log('ERR FORGOT_PASSWORD_MUTATION', e)
            const message = getGQLErrorMessage(e)
                || 'There was an error, please try again later'

            setSnackProps({ message, severity: 'error' })
        }
    }

    return (
        <>

            <Head key="head">
                <title>{process.env.NEXT_PUBLIC_SITE_TITLE} - Reset password</title>
            </Head>

            <Form
                onSubmit={onSubmit}
                initialValues={initialValues}
                validate={validateEmail}
                render={(props) => {
                    const { handleSubmit, form, submitting, pristine, values, invalid } = props
                    // console.log('form props', props)
                    let btnText = submitting ? 'One moment...' : 'Send reset password email'
                    if (submitSuccess) {
                        btnText = 'e-mail sent'
                    }

                    return (
                        <form className={classes.form} onSubmit={handleSubmit} noValidate>
                            <TextField
                                // autoFocus
                                onKeyUp={() => setSubmitSuccess(false)}
                                disabled={submitting}
                                autoComplete="email"
                                variant="outlined" label="e-mail" name="email" required />

                            <Box
                                // buttons row                            
                                display="flex" justifyContent="space-between">

                                <Button disabled={!pristine && (invalid || submitSuccess || submitting)}
                                    size="large"
                                    type="submit"
                                    className={classes.btn}
                                    variant="contained" color="primary">
                                    {btnText}
                                </Button>
                            </Box>
                        </form>
                    )
                }} />


        </>
    );
}

export default withApollo(withLayout(ForgotPasswordPage));

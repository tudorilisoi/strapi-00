import { useMutation } from '@apollo/client';
import { Button, InputAdornment } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { TextField } from 'mui-rff';
import Head from 'next/head';
import Router, { useRouter } from 'next/router';
import { useContext, useState } from 'react';
import { Form } from 'react-final-form';
import { withApollo } from '~/src/apolloUtils';
import { withLayout } from '~/src/components/Layout';
import { useAuth } from '../../src/auth';
import { SnackContext } from '../../src/components/Snack';
import { RESET_PASSWORD_MUTATION } from '../../src/dsl/auth/auth.gql';
import { validateNewPassword } from '../../src/dsl/auth/resetPasswordModel';
import { getGQLErrorMessage, useFormStyles } from '../../src/helpers';


function ResetPasswordPage() {

    return (
        <Box alignSelf="center" width="100%">
            <Container maxWidth="sm" >
                <Box my={4} >
                    <Typography variant="h4" component="h1" gutterBottom >
                        Set a new password
                    </Typography>
                    <ResetPasswordForm />
                </Box>
            </Container>
        </Box>
    )
}

function ResetPasswordForm() {
    const classes = useFormStyles();
    const { login } = useAuth()
    const [resetPasswordMutation, resetPasswordState] = useMutation(RESET_PASSWORD_MUTATION, { errorPolicy: 'none' });
    const router = useRouter()
    const { code } = router.query
    const [showPassword, setShowPassword] = useState(false)
    function toggleShowPassword() {
        setSnackProps({})
        setShowPassword(!showPassword)
    }

    const { setSnackProps } = useContext(SnackContext)
    const [submitSuccess, setSubmitSuccess] = useState(false)

    const initialValues = { password: '', passwordConfirmation: '' }
    async function onSubmit(values) {
        setSnackProps({})
        try {

            const { data: { resetPassword } } = await resetPasswordMutation({
                variables: {
                    ...values,
                    code
                }
            })
            setSnackProps({ message: 'Password set sucessfully!', severity: 'success' })
            login(resetPassword)
            setSubmitSuccess(!!resetPassword)
            //redirect
            Router.push('/')
        } catch (e) {
            console.log('ERR RESET_PASSWORD_MUTATION', e)
            const message = getGQLErrorMessage(e)
                || 'There was an error, please try again later'
            setSnackProps({ message, severity: 'error' })
        }
    }

    return (
        <>

            <Head key="head">
                <title>{process.env.NEXT_PUBLIC_SITE_TITLE} - Reset password</title>
            </Head>

            <Form
                onSubmit={onSubmit}
                initialValues={initialValues}
                validate={validateNewPassword}
                render={(props) => {
                    const { handleSubmit, form, submitting, pristine, values, invalid } = props
                    // console.log('form props', props)
                    let btnText = submitting ? 'One moment...' : 'Change my password'
                    if (submitSuccess) {
                        btnText = 'Your password was changed'
                    }

                    return (
                        <form className={classes.form} onSubmit={handleSubmit} noValidate>
                            <TextField
                                // autoFocus
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <Button className={classes.showPassword} onClick={toggleShowPassword}>
                                                {showPassword ? <VisibilityOff /> : <Visibility />}
                                            </Button>
                                        </InputAdornment>)
                                }}
                                type={showPassword ? 'text' : 'password'}
                                disabled={submitting}
                                autoComplete="current-password"
                                variant="outlined" label="password" name="password" required />

                            <TextField
                                // autoFocus

                                type={showPassword ? 'text' : 'password'}
                                disabled={submitting}
                                autoComplete="current-password"
                                variant="outlined" label="confirm password" name="passwordConfirmation" required />

                            <Box
                                // buttons row                            
                                display="flex" justifyContent="space-between">

                                <Button disabled={submitSuccess || (!pristine && (submitting))}
                                    size="large"
                                    type="submit"
                                    className={classes.btn}
                                    variant="contained" color="primary">
                                    {btnText}
                                </Button>
                            </Box>
                        </form>
                    )
                }} />


        </>
    );
}

export default withApollo(withLayout(ResetPasswordPage));

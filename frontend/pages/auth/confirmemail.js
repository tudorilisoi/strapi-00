import { useMutation, useLazyQuery } from '@apollo/client';
import { Button } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { TextField } from 'mui-rff';
import Head from 'next/head';
import Router, { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import { Form } from 'react-final-form';
import { withApollo } from '~/src/apolloUtils';
import { withLayout } from '~/src/components/Layout';
import { useAuth } from '../../src/auth';
import { SnackContext } from '../../src/components/Snack';
import { CONFIRM_EMAIL_MUTATION, ME_QUERY } from '../../src/dsl/auth/auth.gql';
// import { validateNewPassword } from '../../src/dsl/auth/confirmEmailModel';
import { getGQLErrorMessage, useFormStyles } from '../../src/helpers';


function ConfirmEmailPage() {

    return (
        <Box alignSelf="center" width="100%">
            <Container maxWidth="sm" >
                <Box my={4} >
                    <Typography variant="h4" component="h1" gutterBottom >
                        Verify e-mail address
                    </Typography>
                    <ConfirmEmailForm />
                </Box>
            </Container>
        </Box>
    )
}

function ConfirmEmailForm() {
    const classes = useFormStyles();
    const { login, jwt } = useAuth()
    const [loadMe, { called, loading, data }] = useLazyQuery(
        ME_QUERY,
        {
            fetchPolicy: "network-only",
            variables: {},
        }
    );
    const [confirmEmailMutation, confirmEmailState] = useMutation(
        CONFIRM_EMAIL_MUTATION, {
        errorPolicy: 'all',
        // refetch because confirmation user role is not populated

    });
    const router = useRouter()
    const { confirmation: code } = router.query
    const { setSnackProps } = useContext(SnackContext)
    const [submitSuccess, setSubmitSuccess] = useState(false)
    useEffect(() => {
        if (data && submitSuccess) {
            //force login again with user incl. role
            login({ jwt, user: data.me }, true)

            //redirect
            window.setTimeout(() => {
                Router.push('/')
            }, 1000)
        }

    }, [data, submitSuccess])

    const initialValues = { code }
    async function onSubmit(values) {
        console.log(`🚀 ~ onSubmit ~ values`, values)
        setSnackProps({})
        try {

            const { errors, data: { emailConfirmation } } = await confirmEmailMutation({
                variables: {
                    code: values.code
                }
            })
            if (!emailConfirmation) {
                const e = new Error('E-mail could not be confirmed or already confirmed')
                e.errors = errors
                throw e
            }
            setSnackProps({ message: 'E-mail verified!', severity: 'success' })
            setSubmitSuccess(!!emailConfirmation)
            //set JWT
            login(emailConfirmation)

            // load full user incl. role
            await loadMe()


        } catch (e) {
            console.log('ERR CONFIRM_EMAIL_MUTATION', e)
            const message = getGQLErrorMessage(e)
                || 'There was an error, please try again later'
            setSnackProps({ message, severity: 'error' })
        }
    }

    return (
        <>

            <Head key="head">
                <title>{process.env.NEXT_PUBLIC_SITE_TITLE} - Confirm email</title>
            </Head>

            <Form
                onSubmit={onSubmit}
                initialValues={initialValues}
                // validate={validateNewPassword}
                render={(props) => {
                    const { handleSubmit, form, submitting, pristine, values, invalid } = props
                    // console.log('form props', props)
                    let btnText = submitting ? 'One moment...' : 'Verify e-mail'
                    if (submitSuccess) {
                        btnText = 'E-mail verified'
                    }

                    return (
                        <form className={classes.form} onSubmit={handleSubmit} noValidate>


                            <TextField
                                // autoFocus

                                type={'text'}
                                disabled={submitting}
                                autoComplete="current-password"
                                variant="outlined" label="confirmation code" name="code" required />

                            <Box
                                // buttons row                            
                                display="flex" justifyContent="space-between">

                                <Button disabled={submitSuccess || (!pristine && (submitting))}
                                    size="large"
                                    type="submit"
                                    className={classes.btn}
                                    variant="contained" color="primary">
                                    {btnText}
                                </Button>
                            </Box>
                        </form>
                    )
                }} />


        </>
    );
}

export default withApollo(withLayout(ConfirmEmailPage));

import { useMutation } from '@apollo/client';
import { Button, InputAdornment } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
// this loads ALL icons
// import { Visibility, VisibilityOff } from '@material-ui/icons/esm';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { TextField } from 'mui-rff';
import Head from 'next/head';
import Router from 'next/router';
import { useContext, useState } from 'react';
import { Form } from 'react-final-form';
import { withLayout } from '~/src/components/Layout';
import { withApollo } from '~/src/apolloUtils';
import { useAuth } from '../../src/auth';
import LoggedIn from '../../src/components/LoggedIn';
import { SnackContext } from '../../src/components/Snack';
import { setCookie } from '../../src/cookies';
import { REGISTER_USER_MUTATION } from '../../src/dsl/auth/auth.gql';
import { getGQLErrorMessage, useFormStyles } from '../../src/helpers';
import { validateSignup } from '../../src/dsl/auth/signupModel';
import Link from '~/src/Link';


function SignupPage() {
    const loginData = useAuth()
    const [completed, setCompleted] = useState(false)
    if (completed) {
        return (
            <Box alignSelf="center" width="100%">
                <Container maxWidth="sm" >
                    <Box my={4} >
                        <Typography variant="h4" component="h1" gutterBottom >
                            {'Thank you!'}
                        </Typography>
                        <Typography variant="h4" component="h1" gutterBottom >
                            {'Please verify your email to complete the signup'}
                        </Typography>
                    </Box>
                </Container>
            </Box>
        )
    }
    if (loginData.user) {
        return (
            <Box alignSelf="center" width="100%">
                <Container maxWidth="sm" >
                    <Box my={4} >
                        <Typography variant="h4" component="h1" gutterBottom >
                            You are already signed up
                    </Typography>
                        <LoggedIn user={loginData.user} />
                    </Box>
                </Container>
            </Box>
        )
    }
    return (
        <Box alignSelf="center" width="100%">
            <Container maxWidth="sm" >
                <Box my={4} >
                    <Typography variant="h4" component="h1" gutterBottom >
                        Signup
                    </Typography>
                    <SignupForm setCompleted={setCompleted} />
                </Box>
            </Container>
        </Box>
    )
}


function SignupForm({ setCompleted }) {
    const [registerUserMutation, mutationState] = useMutation(REGISTER_USER_MUTATION, { errorPolicy: 'none' });
    const classes = useFormStyles();
    const { login } = useAuth()

    const [showPassword, setShowPassword] = useState(false)
    function toggleShowPassword() {
        setSnackProps({})
        setShowPassword(!showPassword)
    }

    const { setSnackProps } = useContext(SnackContext)

    const initialValues = {

        email: '',
        username: '',
        password: '',
        passwordConfirmation: '',
    }
    async function onSubmit(values) {
        setSnackProps({})
        try {

            const { data } = await registerUserMutation({
                variables: {
                    username: values.username,
                    email: values.email,
                    password: values.password
                }
            })

            // debugger
            // eslint-disable-next-line no-console
            console.log(`User created!`, data);
            const { user } = data.register
            // TODO check e-mail confirmation
            if (user.confirmed) {
                // setCookie('STRAPI_LOGIN', JSON.stringify(data.register));
                login(data.register)
                Router.push('/')
            } else {
                setCompleted(true)
            }
        } catch (e) {

            const message = getGQLErrorMessage(e)
                || 'There was an error, please try again later'
            setSnackProps({ message, severity: 'error' })
        }
    }




    return (
        <>

            <Head key="head">
                <title>{process.env.NEXT_PUBLIC_SITE_TITLE} - Signup</title>
            </Head>

            <Form
                onSubmit={onSubmit}
                initialValues={initialValues}
                validate={validateSignup}
                render={(props) => {
                    const { handleSubmit, form, submitting, pristine, values, invalid } = props
                    // console.log('form props', props)
                    const btnText = submitting ? 'One moment...' : 'Signup'

                    return (
                        <form className={classes.form} onSubmit={handleSubmit} noValidate>
                            <TextField
                                // autoFocus
                                disabled={submitting}
                                autoComplete="email"
                                variant="outlined" label="e-mail" name="email" required />
                            <TextField
                                // autoFocus
                                disabled={submitting}
                                autoComplete="username"
                                variant="outlined" label="username" name="username" required />
                            <TextField
                                // autoFocus
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <Button className={classes.showPassword} onClick={toggleShowPassword}>
                                                {showPassword ? <VisibilityOff /> : <Visibility />}
                                            </Button>
                                        </InputAdornment>)
                                }}
                                type={showPassword ? 'text' : 'password'}
                                disabled={submitting}
                                autoComplete="current-password"
                                variant="outlined" label="password" name="password" required />

                            <TextField
                                // autoFocus

                                type={showPassword ? 'text' : 'password'}
                                disabled={submitting}
                                autoComplete="current-password"
                                variant="outlined" label="confirm password" name="passwordConfirmation" required />

                            <Box display="flex" justifyContent="space-between">
                                <Button disabled={!pristine && (submitting)}
                                    size="large"
                                    type="submit"
                                    className={classes.btn}
                                    variant="contained" color="primary">
                                    {btnText}
                                </Button>

                                <Button
                                    component={Link}
                                    href="/auth/login"
                                    size="small"
                                    variant="text" color="primary"
                                    className={classes.btn}
                                >
                                    {'Already signed up?'}
                                </Button>
                            </Box>
                        </form>
                    )
                }} />


        </>
    );
}

export default withApollo(withLayout(SignupPage));

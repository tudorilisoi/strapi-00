import { useMutation, useQuery } from '@apollo/client';
import { Button, InputAdornment } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
// this loads ALL icons
// import { Visibility, VisibilityOff } from '@material-ui/icons/esm';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { TextField } from 'mui-rff';
import Head from 'next/head';
import Router from 'next/router';
import { useContext, useState } from 'react';
import { Form } from 'react-final-form';
import { withApollo } from '~/src/apolloUtils';
import { useAuth } from '~/src/auth';
import { withLayout } from '~/src/components/Layout';
import Link from '~/src/Link';
import LoggedIn from '../../src/components/LoggedIn';
import { SnackContext } from '../../src/components/Snack';
import { LOGIN_MUTATION, ME_QUERY } from '../../src/dsl/auth/auth.gql';
import { validateLogin } from '../../src/dsl/auth/loginModel';
import { getGQLErrorMessage, useFormStyles } from '../../src/helpers';

function CheckLogin() {
  const { loading, data, error } = useQuery(ME_QUERY, { fetchPolicy: "network-only" });

  if (loading) return (
    <Typography variant="h5" component="h2" gutterBottom >
      Loading...
    </Typography>
  )

  if (error || !data) return <LoginForm />;

  return (
    <LoggedIn user={data.me} />
  )
}
function LoginPage() {

  return (
    <Box alignSelf="center" width="100%">
      <Container maxWidth="sm" >
        <Box my={4} >
          <Typography variant="h4" component="h1" gutterBottom >
            Login
          </Typography>
          <CheckLogin />
        </Box>
      </Container>
    </Box>
  )
}

function LoginForm() {
  const {login} = useAuth()
  const [loginMutation] = useMutation(LOGIN_MUTATION, { errorPolicy: 'none' });
  const [showPassword, setShowPassword] = useState(false)
  const classes = useFormStyles();

  const { setSnackProps } = useContext(SnackContext)

  const initialValues = { email: '', password: '' }
  async function onSubmit(values) {
    setSnackProps({})
    try {

      const { data } = await loginMutation({
        variables: {
          identifier: values.email,
          password: values.password
        }
      })
      // debugger
      // eslint-disable-next-line no-console
      console.log(`Logged in!`);
      login(data.login)
      // setCookie('STRAPI_LOGIN', JSON.stringify(data.login));

      setSnackProps({ message: `Welcome, ${data.login.user.username}!`, severity: 'success' })
      Router.push('/')
    } catch (e) {
      console.log('ERR LOGIN_MUTATION', e)
      const message = getGQLErrorMessage(e)
        || 'There was an error, please try again later'
      setSnackProps({ message, severity: 'error' })
    }
  }
  function toggleShowPassword() {
    setSnackProps({})
    setShowPassword(!showPassword)
  }



  return (
    <>

      <Head key="head">
        <title>{process.env.NEXT_PUBLIC_SITE_TITLE} - Login</title>
      </Head>

      <Form
        onSubmit={onSubmit}
        initialValues={initialValues}
        validate={validateLogin}
        render={(props) => {
          const { handleSubmit, form, submitting, pristine, values, invalid } = props
          // console.log('form props', props)
          const btnText = submitting ? 'One moment...' : 'Login'

          return (
            <form className={classes.form} onSubmit={handleSubmit} noValidate>
              <TextField
                // autoFocus
                disabled={submitting}
                autoComplete="username"
                variant="outlined" label="username or e-mail" name="email" required />
              <TextField
                // autoFocus
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Button className={classes.showPassword} onClick={toggleShowPassword}>
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </Button>
                    </InputAdornment>)
                }}
                type={showPassword ? 'text' : 'password'}
                disabled={submitting}
                autoComplete="current-password"
                variant="outlined" label="password" name="password" required />

              <Box display="flex" justifyContent="space-between">

                <Button disabled={!pristine && (submitting)}
                  size="large"
                  type="submit"
                  variant="contained" color="primary"
                  className={classes.btn}
                >
                  {btnText}
                </Button>
                <Button
                  component={Link}
                  href="/auth/forgotpassword"
                  size="small"
                  variant="text" color="primary"
                  className={classes.btn}
                >
                  Forgot your password?
                </Button>
              </Box>
            </form>
          )
        }} />


    </>
  );
}

export default withApollo(withLayout(LoginPage))


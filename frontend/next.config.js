/* eslint-disable no-param-reassign */
/* eslint-disable dot-notation */
const path = require('path')
// const webpack = require('webpack')

const nextConfig = {
  future: {
    webpack5: true,
  },

  // env: {
  //   NEXT_PUBLIC_BACKEND_URL: process.env.NEXT_PUBLIC_BACKEND_URL,
  // },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  webpack: (config, { dev }) => {
    if (dev) {
      // eslint-disable-next-line no-param-reassign
      // config.devtool = 'cheap-module-source-map'
    }
    // console.dir(config.resolve)
    // eslint-disable-next-line no-param-reassign
    config.resolve.alias['~'] = path.resolve(__dirname)
    // config.resolve.alias['path'] = false
    // delete config.optimization.realContentHash

    // config.resolve.extensions=[ '.mjs', '.js', '.jsx', '.json', '.wasm']

    return config
  }
}

module.exports = nextConfig

if (process.env.ANALYZE === 'true') {

  // eslint-disable-next-line global-require
  const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: true
  })
  module.exports = withBundleAnalyzer(nextConfig)
}


